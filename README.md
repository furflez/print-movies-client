
# Cliente
Este cliente foi desenvolvido em angular 8, utilizando o nebular para estilização e componentes

## Pré requisitos para o funcionamento

Node.js - https://nodejs.org. a versão deve ser maior que 8.x

Npm - Node.js package manager, já vem com o Node.js. versão deve ser maior que 5.x 

## instalação

Após fazer o download dos arquivos, entrar na pasta e realizar o seguinte comando pelo cmd ou terminal

    npm install

este irá configurar e instalar todos os componentes utilizados no desenvolvimento.

## Executando
Para executar o cliente, realizar o seguinte comando pelo cmd ou terminal

    npm start

após a execução ele irá compilar e criar um servidor no endereço

[http://127.0.0.1:4200](http://127.0.0.1:4200/)

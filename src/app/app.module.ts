/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
*/
import { of as observableOf } from 'rxjs/observable/of';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';

import { NbAuthModule, NbPasswordAuthStrategy, NbAuthJWTToken, NbTokenLocalStorage, NbTokenStorage } from './auth/public_api';
import { AuthGuard } from './auth/services/auth-guard.service';
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { RoleProvider } from './role.provider';
import { AuthInterceptor } from './services/auth.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    ThemeModule.forRoot(),

    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),

    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    CoreModule.forRoot(),
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          baseEndpoint: 'http://127.0.0.1:3000/',
          token: {
            class: NbAuthJWTToken,  // <----
            key: 'token'
          },
          register: {
            redirect: {
              success: '/auth/login',
              failure: null, // stay on the same page
            },
          }
        }),
      ],
    }),
    NbSecurityModule.forRoot({
      accessControl: {
        cliente: {
          view: ['dashboard', 'history'],
        },
        atendente: {
          parent: 'client',
          view: ['users']
        },
        gerente: {
          parent: 'user',
          view: '*',
          remove: '*',
        },
      },
    }),
  ],
  providers: [
    AuthGuard,
    {
      provide: NbRoleProvider,
      useClass: RoleProvider
    },
    { provide: NbTokenStorage, useClass: NbTokenLocalStorage },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}

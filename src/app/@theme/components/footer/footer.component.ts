import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Criado com ♥ por <b><a href="https://furflez.com" target="_blank">Furflez</a></b> 2020</span>
    <div class="socials">
      <a href="https://github.com/furflez" target="_blank" class="ion ion-social-github"></a>
      <a href="https://www.facebook.com/furflez" target="_blank" class="ion ion-social-facebook"></a>
      <a href="https://instagram.com/furflez" target="_blank" class="ion ion-social-instagram"></a>
      <a href="https://br.linkedin.com/in/furflez" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private http: HttpClient) { }

  getMovies() {
    return this.http.get(`/movie/`);
  }
  getMovie(id: String) {
    return this.http.get(`/movie/${id}`);
  }
  register(movie: any) {
    return this.http.post(`/movie/`, movie);
  }
  update(movie: any) {
    return this.http.put(`/movie/`, movie);
  }
  remove(movie: any) {
    return this.http.delete(`/movie/${movie._id}`);
  }
}

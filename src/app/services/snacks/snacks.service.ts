import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SnacksService {

  constructor(private http: HttpClient) { }

  getSnacks() {
    return this.http.get(`/snack/`);
  }
  getSnack(id: String) {
    return this.http.get(`/snack/${id}`);
  }
  register(snack: any) {
    return this.http.post(`/snack/`, snack);
  }
  update(snack: any) {
    return this.http.put(`/snack/`, snack);
  }
  remove(snack: any) {
    return this.http.delete(`/snack/${snack._id}`);
  }
}

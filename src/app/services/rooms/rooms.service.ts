import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(private http: HttpClient) { }

  getRooms() {
    return this.http.get(`/room/`);
  }
  getRoom(id: String) {
    return this.http.get(`/room/${id}`);
  }
  register(room: any) {
    return this.http.post(`/room/`, room);
  }
  update(room: any) {
    return this.http.put(`/room/`, room);
  }
  remove(room: any) {
    return this.http.delete(`/room/${room._id}`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get(`/user/`);
  }
  getUser(id: String) {
    return this.http.get(`/user/${id}`);
  }
  register(user: any) {
    return this.http.post(`/user/`, user);
  }
  update(user: any) {
    return this.http.put(`/user/`, user);
  }
  remove(user: any) {
    return this.http.delete(`/user/${user._id}`);
  }
}

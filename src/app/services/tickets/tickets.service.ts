import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor(private http: HttpClient) { }

  getTickets() {
    return this.http.get(`/ticket/`);
  }
  getTicket(id: String) {
    return this.http.get(`/ticket/${id}`);
  }
  getTicketsBySession(id: String) {
    return <any>this.http.get(`/ticket/session/${id}`);
  }
  register(ticket: any) {
    return <any>this.http.post(`/ticket/`, ticket);
  }
  update(ticket: any) {
    return this.http.put(`/ticket/`, ticket);
  }
  remove(ticket: any) {
    return this.http.delete(`/ticket/${ticket._id}`);
  }
}

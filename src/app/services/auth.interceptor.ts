import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from "@angular/common/http";
import { Observable } from "rxjs";
import { NbTokenLocalStorage, NbTokenService, NbTokenStorage, NbAuthService, NbAuthJWTToken, NB_AUTH_INTERCEPTOR_HEADER } from '../auth/public_api';
import { switchMap } from 'rxjs/operators';
import { Injector, Inject } from '@angular/core';

export class AuthInterceptor implements HttpInterceptor {

  constructor(private injector: Injector,
    @Inject(NB_AUTH_INTERCEPTOR_HEADER) protected headerName: string = 'token') { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return this.authService.getToken()
      .pipe(
        switchMap((token: NbAuthJWTToken) => {
          if (token && token.getValue()) {
            req = req.clone({
              setHeaders: {
                [this.headerName]: token.getValue(),
              },
              url: 'http://127.0.0.1:3000' + req.url
            });
          }
          return next.handle(req);
        }),
      );
  }
  protected get authService(): NbAuthService {
    return this.injector.get(NbAuthService);
  }

}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SessionsService {

  constructor(private http: HttpClient) { }

  getSessions() {
    return this.http.get(`/session/`);
  }
  getSession(id: String) {
    return this.http.get(`/session/${id}`);
  }

  getSessionsByDate(date: String) {
    return this.http.get(`/session/by-date/${date}`);
  }
  register(session: any) {
    return this.http.post(`/session/`, session);
  }
  update(session: any) {
    return this.http.put(`/session/`, session);
  }
  remove(session: any) {
    return this.http.delete(`/session/${session._id}`);
  }
}

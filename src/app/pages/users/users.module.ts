import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { Ng2SmartTableModule } from 'ng2-smart-table'
import { NbInputModule, NbCardModule, NbIconModule, NbLayoutModule, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { DeleteUserDialogComponent } from './delete-user-dialog.component';

@NgModule({
  declarations: [UsersComponent, DeleteUserDialogComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    Ng2SmartTableModule,
    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbLayoutModule,
    NbButtonModule,
    NbDialogModule.forChild()
  ],
  entryComponents: [
    DeleteUserDialogComponent
  ]
})
export class UsersModule { }

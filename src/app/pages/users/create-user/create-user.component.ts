import { Component, OnInit, Inject } from '@angular/core';
import { getDeepFromObject } from '../../../auth/helpers';
import { NB_AUTH_OPTIONS } from '../../../auth/public_api';
import { ImageUploaderOptions, FileQueueObject } from 'ngx-image-uploader';
import { UsersService } from '../../../services/users/users.service';
import { NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  constructor(
    protected service: UsersService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private toastrService: NbToastrService,
    private _location: Location
  ) {
    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');
  }

  ngOnInit() {
    this.user.role = 'cliente';
  }


  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  register(): void {
    this.service.register(this.user).subscribe(response => {

      this._location.back();
      this.toastrService.show(`Usuário cadastrado com sucesso!`, `Sucesso`, { status: 'success' });
    })
  }
}

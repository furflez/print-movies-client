import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'ngx-delete-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.scss']
})
export class DeleteUserDialogComponent implements OnInit {

  user: any;
  constructor(private usersService: UsersService, protected ref: NbDialogRef<DeleteUserDialogComponent>) { }

  ngOnInit() {
  }

  dismiss() {
    this.ref.close();
  }
  remove() {
    this.usersService.remove(this.user).subscribe(response => {
      this.ref.close(response);
    })
  }
}

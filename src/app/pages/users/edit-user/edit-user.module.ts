import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditUserRoutingModule } from './edit-user-routing.module';
import { EditUserComponent } from './edit-user.component';
import { NbCardModule, NbInputModule, NbCheckboxModule, NbLayoutModule, NbButtonModule, NbSelectModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditUserComponent],
  imports: [
    CommonModule,
    EditUserRoutingModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbCheckboxModule,
    NbLayoutModule,
    NbButtonModule,
    NbSelectModule,
  ]
})
export class EditUserModule { }

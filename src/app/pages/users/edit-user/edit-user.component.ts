import { Component, OnInit, Inject } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { NB_AUTH_OPTIONS, getDeepFromObject } from '@nebular/auth';
import { NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { NbAuthService, NbAuthJWTToken } from '../../../auth/public_api';

@Component({
  selector: 'ngx-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  loggedUser: any = {};

  constructor(
    protected service: UsersService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private toastrService: NbToastrService,
    private _location: Location,
    private router: Router,
    private authService: NbAuthService) {
    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');

    this.user = this.router.getCurrentNavigation().extras.state.user;
    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.loggedUser = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable 
        }

      });
  }

  ngOnInit() {
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  edit(): void {
    this.service.update(this.user).subscribe(response => {

      this._location.back();
      this.toastrService.show(`Usuário alterado com sucesso!`, `Sucesso`, { status: 'success' });
    })
  }
}

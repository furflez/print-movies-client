import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users/users.service';
import { DeleteUserDialogComponent } from './delete-user-dialog.component';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { NbAuthService, NbAuthJWTToken } from '../../auth/public_api';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private usersService: UsersService,
    private dialogService: NbDialogService,
    private authService: NbAuthService,
    private toastrService: NbToastrService,
    private router: Router) {

    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.user = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable 
        }

      });
  }
  settings = {
    noDataMessage: 'Sem dados para apresentar',
    columns: {
      fullName: {
        title: 'Nome'

      },
      email: {
        title: 'Email'
      },
      role: {
        title: 'Nível de Acesso'
      },
    },
    hideSubHeader: true,
    actions: {
      columnTitle: 'Ações',
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    mode: 'external'
  };

  user: any;

  data: any;
  ngOnInit() {
    this.usersService.getUsers().subscribe(response => {

      this.data = response;
    })
  }

  edit(event: any) {
    this.router.navigateByUrl(`pages/users/edit`, { state: { user: event.data } });
  };
  delete(event: any) {

    if (this.user._id !== event.data._id)
      this.dialogService.open(DeleteUserDialogComponent, { context: { user: event.data } }).onClose.subscribe(result => {
        this.usersService.getUsers().subscribe(response => {
          this.data = response;
          if (result && result.success)
            this.toastrService.show(`Usuário removido com sucesso!`, `Sucesso`, { status: 'success' });
        })
      });

    else
      this.toastrService.show(`Você não pode remover seu próprio usuário!`, `Atenção`, { status: 'warning' });

  }

}

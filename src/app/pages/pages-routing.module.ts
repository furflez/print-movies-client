import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'buy',
      loadChildren: () => import('./dashboard/buy/buy.module')
        .then(m => m.BuyModule),
    },
    {
      path: 'users',
      loadChildren: () => import('./users/users.module')
        .then(m => m.UsersModule),
    },
    {
      path: 'users/create',
      loadChildren: () => import('./users/create-user/create-user.module')
        .then(m => m.CreateUserModule),
    },
    {
      path: 'users/edit',
      loadChildren: () => import('./users/edit-user/edit-user.module')
        .then(m => m.EditUserModule),
    },
    {
      path: 'rooms',
      loadChildren: () => import('./rooms/rooms.module')
        .then(m => m.RoomsModule),
    },
    {
      path: 'rooms/create',
      loadChildren: () => import('./rooms/create-room/create-room.module')
        .then(m => m.CreateRoomModule),
    },
    {
      path: 'rooms/edit',
      loadChildren: () => import('./rooms/edit-room/edit-room.module')
        .then(m => m.EditRoomModule),
    },
    {
      path: 'movies',
      loadChildren: () => import('./movies/movies.module')
        .then(m => m.MoviesModule),
    },

    {
      path: 'movies/create',
      loadChildren: () => import('./movies/create-movie/create-movie.module')
        .then(m => m.CreateMovieModule),
    },
    {
      path: 'movies/edit',
      loadChildren: () => import('./movies/edit-movie/edit-movie.module')
        .then(m => m.EditMovieModule),
    },
    {
      path: 'sessions',
      loadChildren: () => import('./sessions/sessions.module')
        .then(m => m.SessionsModule),
    },

    {
      path: 'sessions/create',
      loadChildren: () => import('./sessions/create-session/create-session.module')
        .then(m => m.CreateSessionModule),
    },
    {
      path: 'sessions/edit',
      loadChildren: () => import('./sessions/edit-session/edit-session.module')
        .then(m => m.EditSessionModule),
    },

    {
      path: 'snacks',
      loadChildren: () => import('./snacks/snacks.module')
        .then(m => m.SnacksModule),
    },
    {
      path: 'reports/movies',
      loadChildren: () => import('./reports/movies/movies.module')
        .then(m => m.MoviesModule),
    },
    {
      path: 'reports/clients',
      loadChildren: () => import('./reports/clients/clients.module')
        .then(m => m.ClientsModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}

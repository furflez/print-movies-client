import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Em Cartaz',
    icon: 'shopping-cart-outline',
    link: '/pages/dashboard',
    home: true,
    data: {
      permission: 'view',
      resource: 'dashboard'
    },
  },
  {
    title: 'Usuarios',
    link: '/pages/users',
    icon: 'person-outline',
    data: {
      permission: 'view',
      resource: 'users'
    },
  },
  {
    title: 'Salas',
    link: '/pages/rooms',
    icon: 'tv-outline',
    data: {
      permission: 'view',
      resource: 'rooms'
    },
  },
  {
    title: 'Filmes',
    link: '/pages/movies',
    icon: 'film-outline',
    data: {
      permission: 'view',
      resource: 'movies'
    },
  },
  {
    title: 'Sessões',
    link: '/pages/sessions',
    icon: 'video-outline',
    data: {
      permission: 'view',
      resource: 'sessions'
    },
  },
  {
    title: 'Snacks',
    link: '/pages/snacks',
    icon: 'shopping-bag-outline',
    data: {
      permission: 'view',
      resource: 'snacks'
    },
  },
  {
    title: 'Relatórios',
    icon: 'bar-chart-outline',
    data: {
      permission: 'view',
      resource: 'reports'
    },
    children: [
      {

        title: 'Filmes',
        link: '/pages/reports/movies',
        icon: 'bar-chart-outline',
        data: {
          permission: 'view',
          resource: 'movies-reports'
        },
      },
      {

        title: 'Clientes',
        link: '/pages/reports/clients',
        icon: 'bar-chart-outline',
        data: {
          permission: 'view',
          resource: 'clients-reports'
        },
      }
    ]
  },
];

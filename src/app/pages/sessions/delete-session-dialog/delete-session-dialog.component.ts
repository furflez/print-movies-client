import { Component, OnInit } from '@angular/core';
import { SessionsService } from '../../../services/sessions/sessions.service';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-delete-session-dialog',
  templateUrl: './delete-session-dialog.component.html',
  styleUrls: ['./delete-session-dialog.component.scss']
})
export class DeleteSessionDialogComponent implements OnInit {

  session: any = {};
  constructor(private sessionsService: SessionsService, protected ref: NbDialogRef<DeleteSessionDialogComponent>) { }

  ngOnInit() {
  }

  dismiss() {
    this.ref.close();
  }
  remove() {
    this.sessionsService.remove(this.session).subscribe(response => {
      this.ref.close(response);
    })
  }
}
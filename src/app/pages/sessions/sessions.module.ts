import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionsRoutingModule } from './sessions-routing.module';
import { SessionsComponent } from './sessions.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbInputModule, NbCardModule, NbIconModule, NbLayoutModule, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { DeleteSessionDialogComponent } from './delete-session-dialog/delete-session-dialog.component';

@NgModule({
  declarations: [SessionsComponent, DeleteSessionDialogComponent],
  imports: [
    CommonModule,
    SessionsRoutingModule,
    Ng2SmartTableModule,

    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbLayoutModule,
    NbButtonModule,
    NbDialogModule.forChild()
  ],
  entryComponents: [DeleteSessionDialogComponent]
})
export class SessionsModule { }

import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { DeleteSessionDialogComponent } from './delete-session-dialog/delete-session-dialog.component';
import { SessionsService } from '../../services/sessions/sessions.service';

@Component({
  selector: 'ngx-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {
  settings = {
    noDataMessage: 'Sem dados para apresentar',
    columns: {
      date: {
        title: 'Data',
        filter: true,

        valuePrepareFunction: (date: any) => {
          date = new Date(date);
          return `${date.getDate() + 1 < 10 ? '0' : ''}${date.getDate()}/
          ${date.getMonth() + 1 < 10 ? '0' : ''}${date.getMonth() + 1}/
          ${date.getFullYear()}`;
        }
      },
      time: {
        title: 'Horário',
        filter: true,
        valuePrepareFunction: (cell, row) => {
          const date = new Date(row.date);
          return `${date.getHours() < 10 ? '0' : ''}${date.getHours()}:${date.getMinutes() < 10 ? '0' : ''}${date.getMinutes()}`;
        }
      },
    },
    movie: {
      title: 'Filme',
      valuePrepareFunction: (cell, row) => {
        debugger
        return row.movie.title;
      }
    },
    room: {
      title: 'Sala',
      valuePrepareFunction: (cell, row) => {
        debugger
        return row.room.name;
      }
    },

    hideSubHeader: true,
    actions: {
      columnTitle: 'Ações',
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    mode: 'external'
  };

  data: any = []
  constructor(private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private router: Router,
    private sessionsService: SessionsService) { }

  ngOnInit() {
    this.sessionsService.getSessions().subscribe(response => {
      this.data = response;
    }
    )
  }


  edit(event: any) {
    this.router.navigateByUrl(`pages/sessions/edit`, { state: { session: event.data } });
  };
  delete(event: any) {

    this.dialogService.open(DeleteSessionDialogComponent, { context: { session: event.data } }).onClose.subscribe(result =>
      this.sessionsService.getSessions().subscribe(response => {
        this.data = response;
        if (result && result.success)
          this.toastrService.show(`Usuário removido com sucesso!`, `Sucesso`, { status: 'success' });
      }));
  }

}

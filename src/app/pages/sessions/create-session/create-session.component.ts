import { Component, OnInit, Inject } from '@angular/core';
import { getDeepFromObject } from '../../../auth/helpers';
import { NB_AUTH_OPTIONS } from '../../../auth/public_api';
import { NbToastrService, NbDateService } from '@nebular/theme';
import { Location } from '@angular/common';
import { MoviesService } from '../../../services/movies/movies.service';
import { RoomsService } from '../../../services/rooms/rooms.service';
import { SessionsService } from '../../../services/sessions/sessions.service';

@Component({
  selector: 'ngx-create-session',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.scss']
})
export class CreateSessionComponent implements OnInit {
  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  session: any = {};
  rooms: any;
  movies: any;
  min: Date;
  constructor(

    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private toastrService: NbToastrService,
    private _location: Location,
    private roomsService: RoomsService,
    private sessionsService: SessionsService,
    private moviesService: MoviesService,
    protected dateService: NbDateService<Date>
  ) {
    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');

    this.roomsService.getRooms().subscribe(rooms => this.rooms = rooms);
    this.moviesService.getMovies().subscribe(movies => this.movies = movies);

    this.min = this.dateService.addDay(this.dateService.today(), 0);
  }

  ngOnInit() {
  }


  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  register(): void {

    this.session.date = new Date(this.session.date.getFullYear(), this.session.date.getMonth(), this.session.date.getDate(), this.session.time.split(':')[0], this.session.time.split(':')[1])
    this.sessionsService.register(this.session).subscribe(response => {

      this._location.back();
      this.toastrService.show(`Usuário cadastrado com sucesso!`, `Sucesso`, { status: 'success' });
    })
  }
}

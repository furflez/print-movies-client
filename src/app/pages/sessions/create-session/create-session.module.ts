import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateSessionRoutingModule } from './create-session-routing.module';
import { CreateSessionComponent } from './create-session.component';
import { NbCardModule, NbInputModule, NbCheckboxModule, NbLayoutModule, NbButtonModule, NbSelectModule, NbDatepickerModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateSessionComponent],
  imports: [
    CommonModule,
    CreateSessionRoutingModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbCheckboxModule,
    NbLayoutModule,
    NbButtonModule,
    NbSelectModule,
    NbDatepickerModule,
  ]
})
export class CreateSessionModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditSessionComponent } from './edit-session.component';

const routes: Routes = [
  {
    path: '',
    component: EditSessionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditSessionRoutingModule { }

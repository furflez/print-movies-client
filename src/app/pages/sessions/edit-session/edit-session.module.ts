import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditSessionRoutingModule } from './edit-session-routing.module';
import { EditSessionComponent } from './edit-session.component';
import { NbCardModule, NbInputModule, NbCheckboxModule, NbLayoutModule, NbButtonModule, NbSelectModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditSessionComponent],
  imports: [
    CommonModule,
    EditSessionRoutingModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbCheckboxModule,
    NbLayoutModule,
    NbButtonModule,
    NbSelectModule,
  ]
})
export class EditSessionModule { }

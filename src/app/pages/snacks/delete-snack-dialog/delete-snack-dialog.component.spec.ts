import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSnackDialogComponent } from './delete-snack-dialog.component';

describe('DeleteSnackDialogComponent', () => {
  let component: DeleteSnackDialogComponent;
  let fixture: ComponentFixture<DeleteSnackDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSnackDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSnackDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

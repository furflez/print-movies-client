import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateSnackRoutingModule } from './create-snack-routing.module';
import { CreateSnackComponent } from './create-snack.component';

@NgModule({
  declarations: [CreateSnackComponent],
  imports: [
    CommonModule,
    CreateSnackRoutingModule
  ]
})
export class CreateSnackModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SnacksRoutingModule } from './snacks-routing.module';
import { SnacksComponent } from './snacks.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbInputModule, NbCardModule, NbIconModule, NbLayoutModule, NbButtonModule } from '@nebular/theme';
import { DeleteSnackDialogComponent } from './delete-snack-dialog/delete-snack-dialog.component';

@NgModule({
  declarations: [SnacksComponent, DeleteSnackDialogComponent],
  imports: [
    CommonModule,
    SnacksRoutingModule,
    Ng2SmartTableModule,

    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbLayoutModule,
    NbButtonModule
  ]
})
export class SnacksModule { }

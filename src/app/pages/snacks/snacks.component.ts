import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-snacks',
  templateUrl: './snacks.component.html',
  styleUrls: ['./snacks.component.scss']
})
export class SnacksComponent implements OnInit {

  constructor() { }
  settings = {
    noDataMessage: 'Sem dados para apresentar',
    columns: {
      image: {
        title: 'Imagem',
        type: 'html',
        valuePrepareFunction: (image: string) => { return `<img width="100px" src="${image}" />`; }

      },
      name: {
        title: 'Nome'
      },
    },
    price: {
      title: 'Preço'
    },


    hideSubHeader: true,
    actions: {
      columnTitle: 'Ações',
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    }
  };

  data = [
    {
      image: 'https://www.bigfesta.com.br/resizer/view/373/373/false/true/5341.jpg',
      name: 'Pipoca Salgada',
      price: 'R$ 15,00',
    },

  ]
  ngOnInit() {
  }

}

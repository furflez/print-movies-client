import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditSnackRoutingModule } from './edit-snack-routing.module';
import { EditSnackComponent } from './edit-snack.component';

@NgModule({
  declarations: [EditSnackComponent],
  imports: [
    CommonModule,
    EditSnackRoutingModule
  ]
})
export class EditSnackModule { }

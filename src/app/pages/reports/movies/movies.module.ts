import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent, FsIconComponent } from './movies.component';
import { NbTreeGridModule, NbCardModule, NbIconModule, NbThemeModule } from '@nebular/theme';

@NgModule({
  declarations: [MoviesComponent, FsIconComponent],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbThemeModule
  ]
})
export class MoviesModule { }

import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { RoomsService } from '../../../services/rooms/rooms.service';

@Component({
  selector: 'ngx-delete-room-dialog',
  templateUrl: './delete-room-dialog.component.html',
  styleUrls: ['./delete-room-dialog.component.scss']
})
export class DeleteRoomDialogComponent implements OnInit {

  room: any = {};
  constructor(private roomsService: RoomsService, protected ref: NbDialogRef<DeleteRoomDialogComponent>) { }

  ngOnInit() {
  }

  dismiss() {
    this.ref.close();
  }
  remove() {
    this.roomsService.remove(this.room).subscribe(response => {
      this.ref.close(response);
    })
  }
}
import { Component, OnInit, Inject } from '@angular/core';
import { getDeepFromObject } from '../../../auth/helpers';
import { NB_AUTH_OPTIONS } from '../../../auth/public_api';
import { NbToastrService } from '@nebular/theme';
import { RoomsService } from '../../../services/rooms/rooms.service';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.scss']
})
export class CreateRoomComponent implements OnInit {
  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  room: any = {};
  chairs: any = [];

  alphabet: string[] = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N',
    'O', 'P', 'Q', 'R', 'S', 'T', 'U',
    'V', 'W', 'X', 'Y', 'Z'];

  constructor(
    protected service: RoomsService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private toastrService: NbToastrService,
    private _location: Location
  ) {
    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');

    this.room.lines = 6;
    this.room.columns = 9;
  }

  ngOnInit() {
  }

  chairClick() {

  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  register(): void {

    for (let i = 0; i < this.room.lines; i++) {
      for (let j = 0; j < this.room.columns; j++) {
        this.chairs.push(`${this.alphabet[i] + j}`)
      }
    }
    this.room.chairs = this.chairs;

    this.service.register(this.room).subscribe(response => {

      this._location.back();
      this.toastrService.show(`Sala cadastrada com sucesso!`, `Sucesso`, { status: 'success' });
    })
  }


}

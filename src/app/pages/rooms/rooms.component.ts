import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../../services/rooms/rooms.service';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { NbAuthService } from '../../auth/public_api';
import { Router } from '@angular/router';
import { DeleteRoomDialogComponent } from './delete-room-dialog/delete-room-dialog.component';

@Component({
  selector: 'ngx-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  constructor(private roomsService: RoomsService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private router: Router) { }

  settings = {
    noDataMessage: 'Sem dados para apresentar',
    columns: {
      name: {
        title: 'Nome',
        filter: true

      },
      chairs: {
        title: 'Assentos',
        filter: true,
        valuePrepareFunction: (chairs: string[]) => { return chairs.length }
      },
    },
    hideSubHeader: true,
    actions: {
      columnTitle: 'Ações',
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    mode: 'external'
  };

  data: any = [];
  ngOnInit() {

    this.roomsService.getRooms().subscribe(response => {
      this.data = response;
    }

    )
  };

  edit(event: any) {
    this.router.navigateByUrl(`pages/rooms/edit`, { state: { room: event.data } });
  };
  delete(event: any) {

    this.dialogService.open(DeleteRoomDialogComponent, { context: { room: event.data } }).onClose.subscribe(result =>
      this.roomsService.getRooms().subscribe(response => {
        this.data = response;
        if (result && result.success)
          this.toastrService.show(`Usuário removido com sucesso!`, `Sucesso`, { status: 'success' });
      }));
  }

}

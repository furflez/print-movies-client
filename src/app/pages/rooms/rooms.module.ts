import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomsRoutingModule } from './rooms-routing.module';
import { RoomsComponent } from './rooms.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule, NbInputModule, NbIconModule, NbLayoutModule, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { DeleteRoomDialogComponent } from './delete-room-dialog/delete-room-dialog.component';

@NgModule({
  declarations: [RoomsComponent, DeleteRoomDialogComponent],
  imports: [
    CommonModule,
    RoomsRoutingModule,
    Ng2SmartTableModule,

    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbLayoutModule,
    NbButtonModule,
    NbDialogModule.forChild()
  ],
  entryComponents: [
    DeleteRoomDialogComponent
  ]
})
export class RoomsModule { }

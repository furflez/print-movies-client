import { Component, OnInit, Inject } from '@angular/core';
import { NB_AUTH_OPTIONS, getDeepFromObject } from '../../../auth/public_api';
import { NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { RoomsService } from '../../../services/rooms/rooms.service';

@Component({
  selector: 'ngx-edit-room',
  templateUrl: './edit-room.component.html',
  styleUrls: ['./edit-room.component.scss']
})
export class EditRoomComponent implements OnInit {

  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  room: any = {};
  chairs: any = [];

  alphabet: string[] = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N',
    'O', 'P', 'Q', 'R', 'S', 'T', 'U',
    'V', 'W', 'X', 'Y', 'Z'];

  constructor(
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private toastrService: NbToastrService,
    private _location: Location,
    private router: Router,
    private authService: NbAuthService,
    private roomsService: RoomsService) {
    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');


    this.room = this.router.getCurrentNavigation().extras.state.room;


  }

  ngOnInit() {
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  edit(): void {
    for (let i = 0; i < this.room.lines; i++) {
      for (let j = 0; j < this.room.columns; j++) {
        this.chairs.push(`${this.alphabet[i] + j}`)
      }
    }
    this.room.chairs = this.chairs;
    this.roomsService.update(this.room).subscribe(response => {

      this._location.back();
      this.toastrService.show(`Sala alterada com sucesso!`, `Sucesso`, { status: 'success' });
    })
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoomRoutingModule } from './edit-room-routing.module';
import { EditRoomComponent } from './edit-room.component';
import { NbCardModule, NbInputModule, NbCheckboxModule, NbLayoutModule, NbButtonModule, NbSelectModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditRoomComponent],
  imports: [
    CommonModule,
    EditRoomRoutingModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbCheckboxModule,
    NbLayoutModule,
    NbButtonModule,
    NbSelectModule,
  ]
})
export class EditRoomModule { }

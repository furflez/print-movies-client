import { Component, OnInit } from '@angular/core';
import { NbDateService } from '@nebular/theme';
import { SessionsService } from '../../services/sessions/sessions.service';
import { Router } from '@angular/router';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {


  days: Date[] = [];
  selectedDate: Date;

  sessions: any;

  constructor(private dateService: NbDateService<Date>, private sessionsService: SessionsService,
    private router: Router) {
    this.selectedDate = new Date();
    this.days.push(new Date());
  };

  ngOnInit() {

    for (let index = 1; index < 6; index++) {
      this.days.push(this.dateService.addDay(this.selectedDate, index));

    }

    this.selectDay(this.selectedDate);

  }
  selectDay(day: Date) {
    this.selectedDate = day;
    day.setHours(12);
    this.sessionsService.getSessionsByDate(day.toISOString()).subscribe(result => {
      this.sessions = result;
    })
  }

  getNextDays() {
    this.days.shift();
    const lastDate = this.days[this.days.length - 1];
    this.days.push(this.dateService.addDay(lastDate, 1));
  }

  getBackDays() {

    const lastDate = this.days[0];
    if (this.dateService.compareDates(lastDate, new Date()) > 0) {
      this.days.pop();
      this.days.unshift(this.dateService.addDay(lastDate, -1));
    }
  }
  verifyToday(day: Date) {
    day = new Date(day.getFullYear(), day.getMonth(), day.getDate());
    const today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    return this.dateService.compareDates(day, today) === 0;


  }
  verifySelected(day: Date) {
    day = new Date(day.getFullYear(), day.getMonth(), day.getDate());
    return this.dateService.compareDates(day, new Date(this.selectedDate.getFullYear(), this.selectedDate.getMonth(), this.selectedDate.getDate())) === 0;

  }
  goToBuy(session: any) {
    this.router.navigateByUrl(`pages/buy`, { state: { session: session } });
  }

  timeConvert(n: any) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + " hora(s) e " + rminutes + " minuto(s).";
  }
}

import { Component, OnInit } from '@angular/core';
import { SessionsService } from '../../../services/sessions/sessions.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbAuthService, NbAuthJWTToken } from '../../../auth/public_api';
import { TicketsService } from '../../../services/tickets/tickets.service';
import { Location } from '@angular/common';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.scss']
})
export class BuyComponent implements OnInit {

  user: any;
  session: any;
  chairsForm: FormGroup;
  selectedSnacks: any[] = [];

  purchasedChairs: any[] = [];
  selectedChairs: any[] = [];
  alphabet: string[] = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N',
    'O', 'P', 'Q', 'R', 'S', 'T', 'U',
    'V', 'W', 'X', 'Y', 'Z'];

  endDate: Date;


  constructor(private sessionsService: SessionsService,
    private router: Router, private fb: FormBuilder,
    private authService: NbAuthService,
    private ticketsService: TicketsService,
    private toastrService: NbToastrService,
    private _location: Location) {
    this.session = this.router.getCurrentNavigation().extras.state.session;
    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.user = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable 
        }

      });
  }

  ngOnInit() {

    this.sessionsService.getSession(this.session._id).subscribe(result => {
      this.session = result;

      this.endDate = new Date(this.session.date);
      this.endDate.setMinutes(this.endDate.getMinutes() + this.session.movie.duration);

    })
    this.ticketsService.getTicketsBySession(this.session._id).subscribe(result => {
      if (result.length > 0)
        result.forEach(ticket => {
          ticket.chairs.forEach(element => {
            this.purchasedChairs.push(element);
          });
          console.log(this.purchasedChairs);
        });

    })
    this.chairsForm = this.fb.group({
      chairs: ['', Validators.required],
    });
  }

  isChairPurchased(x: any, y: any) {
    const chairName = `${this.alphabet[x]}${y + 1}`;
    return this.purchasedChairs.indexOf(chairName) > -1;
  }
  selectChair(x: any, y: any) {
    const chairName = `${this.alphabet[x]}${y + 1}`;
    const index = this.selectedChairs.indexOf(chairName);

    if (index > -1) {
      this.selectedChairs.splice(index, 1);
    } else {
      this.selectedChairs.push(chairName);
    }

  }
  isSelected(x: any, y: any) {
    const chairName = `${this.alphabet[x]}${y + 1}`;

    return this.selectedChairs.indexOf(chairName) > -1;

  }


  getFormattedPrice(price: number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
  }

  snacksSum() {
    return 0;
  }

  finalizePurchase() {

    const ticket = {

      session: this.session,
      user: this.user,
      snacks: this.selectedSnacks,
      chairs: this.selectedChairs

    }

    this.ticketsService.register(ticket).subscribe(result => {

      if (result.success) {

        this._location.back();
        this.toastrService.show(`Ingresso comprado com sucesso!`, `Sucesso`, { status: 'success' });
      } else {
        this.toastrService.show(`Ocorreu um problema ao processar sua compra, tente novamente mais tarde`, `Falha`, { status: 'warning' });
      }
    });

  }

  timeConvert(n: any) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + " hora(s) e " + rminutes + " minuto(s).";
  }
}

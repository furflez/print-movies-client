import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../../services/movies/movies.service';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-delete-movie-dialog',
  templateUrl: './delete-movie-dialog.component.html',
  styleUrls: ['./delete-movie-dialog.component.scss']
})
export class DeleteMovieDialogComponent implements OnInit {

  movie: any = {};
  constructor(private moviesService: MoviesService, protected ref: NbDialogRef<DeleteMovieDialogComponent>) { }

  ngOnInit() {
  }

  dismiss() {
    this.ref.close();
  }
  remove() {
    this.moviesService.remove(this.movie).subscribe(response => {
      this.ref.close(response);
    })
  }
}
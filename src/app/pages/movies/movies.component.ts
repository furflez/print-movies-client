import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { MoviesService } from '../../services/movies/movies.service';
import { DeleteMovieDialogComponent } from './delete-movie-dialog/delete-movie-dialog.component';

@Component({
  selector: 'ngx-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  constructor(
    private moviesService: MoviesService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private router: Router) { }

  settings = {
    noDataMessage: 'Sem dados para apresentar',
    columns: {
      image: {
        title: 'Imagem',
        type: 'html',
        valuePrepareFunction: (image: string) => { return `<img width="100px" src="${image}" />`; }
      },
      title: {
        title: 'Título',
        filter: true
      },
      description: {
        title: 'Descrição',
        filter: true
      },
      duration: {
        title: 'Duração',
        filter: true,
      },
      animationType: {
        title: 'Tipo de Animação',
        filter: true,
      },
      audioType: {
        title: 'Tipo de Áudio',
        filter: true,
      },
    },
    hideSubHeader: true,
    actions: {
      columnTitle: 'Ações',
      position: 'right'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    mode: 'external'

  };

  data: any = [];



  ngOnInit() {

    this.moviesService.getMovies().subscribe(response => {
      this.data = response;
    }

    )
  };

  edit(event: any) {
    this.router.navigateByUrl(`pages/movies/edit`, { state: { movie: event.data } });
  };
  delete(event: any) {

    this.dialogService.open(DeleteMovieDialogComponent, { context: { movie: event.data } }).onClose.subscribe(result =>
      this.moviesService.getMovies().subscribe(response => {
        this.data = response;
        if (result && result.success)
          this.toastrService.show(`Usuário removido com sucesso!`, `Sucesso`, { status: 'success' });
      }));
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { MoviesService } from '../../../services/movies/movies.service';
import { NB_AUTH_OPTIONS, getDeepFromObject } from '@nebular/auth';
import { NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { NbAuthService, NbAuthJWTToken } from '../../../auth/public_api';

@Component({
  selector: 'ngx-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.scss']
})
export class EditMovieComponent implements OnInit {

  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  movie: any = {};
  loggedMovie: any = {};

  constructor(
    protected service: MoviesService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private toastrService: NbToastrService,
    private _location: Location,
    private router: Router,
    private authService: NbAuthService) {
    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');

    this.movie = this.router.getCurrentNavigation().extras.state.movie;
    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.loggedMovie = token.getPayload(); // here we receive a payload from the token and assigns it to our `movie` variable 
        }

      });
  }

  ngOnInit() {
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  edit(): void {
    this.service.update(this.movie).subscribe(response => {

      this._location.back();
      this.toastrService.show(`Filme alterado com sucesso!`, `Sucesso`, { status: 'success' });
    })
  }
}

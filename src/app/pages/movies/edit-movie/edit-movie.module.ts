import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditMovieRoutingModule } from './edit-movie-routing.module';
import { EditMovieComponent } from './edit-movie.component';
import { NbCardModule, NbInputModule, NbCheckboxModule, NbLayoutModule, NbButtonModule, NbSelectModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditMovieComponent],
  imports: [
    CommonModule,
    EditMovieRoutingModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbCheckboxModule,
    NbLayoutModule,
    NbButtonModule,
    NbSelectModule,
  ]
})
export class EditMovieModule { }

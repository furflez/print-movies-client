import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { NbCardModule, NbInputModule, NbIconModule, NbLayoutModule, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { DeleteMovieDialogComponent } from './delete-movie-dialog/delete-movie-dialog.component';

@NgModule({
  declarations: [MoviesComponent, DeleteMovieDialogComponent],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    Ng2SmartTableModule,

    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbLayoutModule,
    NbButtonModule,
    NbDialogModule.forChild()
  ],
  entryComponents: [
    DeleteMovieDialogComponent
  ]
})
export class MoviesModule { }

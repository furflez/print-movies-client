import { Component, OnInit, Inject } from '@angular/core';
import { getDeepFromObject } from '../../../auth/helpers';
import { NB_AUTH_OPTIONS } from '../../../auth/public_api';
import { ImageUploaderOptions, FileQueueObject } from 'ngx-image-uploader';
import { MoviesService } from '../../../services/movies/movies.service';
import { NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-create-movie',
  templateUrl: './create-movie.component.html',
  styleUrls: ['./create-movie.component.scss']
})
export class CreateMovieComponent implements OnInit {
  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  movie: any = {};
  constructor(
    protected service: MoviesService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private toastrService: NbToastrService,
    private _location: Location
  ) {
    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');
  }

  ngOnInit() {
  }


  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  register(): void {
    this.service.register(this.movie).subscribe(response => {

      this._location.back();
      this.toastrService.show(`Filme cadastrado com sucesso!`, `Sucesso`, { status: 'success' });
    })
  }
}

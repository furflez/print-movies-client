import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateMovieRoutingModule } from './create-movie-routing.module';
import { CreateMovieComponent } from './create-movie.component';
import { NbCardModule, NbInputModule, NbCheckboxModule, NbLayoutModule, NbButtonModule, NbSelectModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateMovieComponent],
  imports: [
    CommonModule,
    CreateMovieRoutingModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbCheckboxModule,
    NbLayoutModule,
    NbButtonModule,
    NbSelectModule,
  ]
})
export class CreateMovieModule { }
